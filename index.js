const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const path = require('path');


const passport = require('passport');
const Oauth2 = require('passport-oauth2');

const port = 45349;
const app = express();

app.use(express.static("public"));
app.use(session({secret: 'secretehas1111333', maxAge: null, resave: false, saveUninitialized: false})); // TODO: Use appId as secrete key here
app.use(bodyParser.urlencoded({extended: false}));
app.use(passport.initialize());
app.use(passport.session());

app.get('/', (req, res) => {
    res.send('success');
    console.log('req served');
});

app.listen(port);
console.log('app started on port', port);


Oauth2.prototype.parseErrorResponse = function(body, status) {
    var json = JSON.parse(body);

    if (json.error) {
        json.status = status;
        console.log('[Oauth2] Error response received', json);

        return JSON.stringify(json);
    }

    return null;
};

passport.serializeUser(function(user, cb) {
    cb(null, user);
});

passport.deserializeUser(function(id, cb) {
    cb(null, id);
});

passport.use('oauth2', new Oauth2({
        authorizationURL: 'http://78.93.17.11/connect/authorize',
        tokenURL: 'http://78.93.17.11/connect/token',
        clientID: 'MubasherClient2021',
        clientSecret: 'Mubasher2021',
        scope: 'TradingAPI.All',
        callbackURL: `http://localhost:${port}/callback`,
        pkce: true,
        state: true,
        passReqToCallback: true
    },
    function (req, accessToken, refreshToken, profile, cb) {
        console.log('verify token called', accessToken);

        cb(null, profile);
    }
));

app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

// Redirect the user to the OAuth 2.0 provider for authentication.  When
// complete, the provider will redirect the user back to the application at
app.get('/oauth2', passport.authenticate('oauth2', /*{state: 'stateFromApp'}*/)); // Can send the scope here - { scope: 'someId from URL' }

// Oauth fail handling
app.get('/oauthFail', (req, res) => {
    res.status(404).json({message: 'error in authentication'});
});
//
// Oauth fail handling
app.get('/oauthSuccess', (req, res) => {
    res.sendFile(path.join(__dirname, '/success.html'));
});
// The OAuth 2.0 provider has redirected the user back to the application.
// Finish the authentication process by attempting to obtain an access
// token.  If authorization was granted, the user will be logged in.
// Otherwise, authentication has failed.
app.get('/callback', passport.authenticate('oauth2', {
    successRedirect: '/oauthSuccess',
    failureRedirect: '/oauthFail'
}));
